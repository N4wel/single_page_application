import { Component, OnInit, Input, HostBinding, EventEmitter, Output } from '@angular/core';   //Bibliotecas. Input: permite importar datos
import { DestinoViaje } from '../models/viaje-destino.model';

@Component({
  selector: 'app-viaje-destino',
  templateUrl: './viaje-destino.component.html',
  styleUrls: ['./viaje-destino.component.css']
})

export class ViajeDestinoComponent implements OnInit {

  @Input() destino!: DestinoViaje;
  @HostBinding('attr.class') cssClass = 'col-md-4';
  @Output() clicked: EventEmitter<DestinoViaje>;
  constructor() { 
    this.clicked = new EventEmitter();
  }

  ngOnInit(): void {
  }
  ir(){
    this.clicked.emit(this.destino);
    return false;
  }

}
