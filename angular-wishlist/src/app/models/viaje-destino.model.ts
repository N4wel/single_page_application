export class DestinoViaje {
    private selected: boolean;

    constructor(public nombre: string, u: string) {
        this.selected =false;
    }

    isSelected(): boolean{
        return this.selected;
    }
    setSelected(s:boolean):boolean{
        return this.selected = s;
    }

}