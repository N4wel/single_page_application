Proyecto SPA (Single Page Application) con Angular

Contribuciones:
Si estas interesado en contribuir con este repositorio descarga. Debes utilizar node (16.13.0 o superior) y npm (6.14.14 o superior). Para iniciar a contruibuir realiza las siguitnes acciones:

> git clone https://N4wel@bitbucket.org/N4wel/single_page_application.git 

> cd single_page_application

> npm install

> npm run

o

> npm run dev

Deberías tener previamente instalado nodejs. Si requeres intalarlo sigue los siguitnes pasos:
1)  Instala NVM (Node Version Manager).
https://github.com/coreybutler/nvm-windows/blob/master/README.md
Verifica la versión instalada con el comando:
> nvm version

2)  Instala Node desde NVM, esto instala NPM. Utilizá un CMD con privilegios de administrador para que se puedan crear los enlaces simbólicos a Node

> nvm install	//Instala última versión estable de node y npm

o

> nvm install <versión>	//Instala la versión seleccioanda de node y su repositorio npm asociado

> nvm list	//Lista las versiones instaladas en el equipo, la versión utilizada debe estar precedida por un asterisco

> nvm use <versión>	//Selecciona o modifica la versión utilizada

> node -v	//Lista la versión de nodejs

> npm -v		//Lista la versión de npm

Owner: Carlos Ceron

Proyecto SPA 